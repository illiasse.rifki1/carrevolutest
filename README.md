# Carrevolutest
This application was made for a technical assessment for the company "Carrevolutis" based in PARIS.

## 1 - Installing the dependencies
``` cd ./Carrevolutest/backend ```  
``` npm install ```

``` cd ./Carrevolutest/frontend ```  
``` npm install ```

## 2 - Start the application
``` cd Carrevolutest ```  
``` docker-compose up```