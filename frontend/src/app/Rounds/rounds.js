import React, { Component } from 'react';

//Material-ui import
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';

//Watson import
import synthesize from 'watson-speech/text-to-speech/synthesize';
import recognizeFile from 'watson-speech/speech-to-text/recognize-file';

import classNames from 'classnames';

const styles = theme => ({
    roundDiv: {
        backgroundColor: "transparent",
        margin: 0,
        marginTop: "1%",
        width: "50%",
        float: "right",
        marginRight: "10%",
        textAlign: "center",

    },
    button: {
        margin: 0,
        textAlign: "center",
    },

    outputSentence:
        {
            color: "#9A4600",
            fontSize: "14px",
        },
    bootstrapRoot: {
        boxShadow: 'none',
        textTransform: 'none',
        fontSize: 25,
        marginTop: "1%",
        textAlign: "center",
        padding: '12px',
        backgroundColor: '#F6CF70',
        borderRadius: "0",
        borderColor: '#9A4600',
        fontFamily: [
            '-apple-system',
            'BlinkMacSystemFont',
            '"Segoe UI"',
            'Roboto',
            '"Helvetica Neue"',
            'Arial',
            'sans-serif',
            '"Apple Color Emoji"',
            '"Segoe UI Emoji"',
            '"Segoe UI Symbol"',
        ].join(','),
        '&:hover': {
            backgroundColor: '#9A4600',
            borderColor: '#F6CF70',
        },
        '&:active': {
            boxShadow: 'none',
            backgroundColor: '#0062cc',
            borderColor: '#005cbf',
        },
        '&:focus': {
            boxShadow: '0 0 0 0.2rem rgba(0,123,255,.5)',
        },
    },
});

class Rounds extends Component {
    constructor(props) {
        super(props);
        this.state = {
            nbOfIterations: this.props.nbOfIterations,
            audio: null,
            userSentence: this.props.userSentence,
            userName: this.props.userName,
            peopleSentences: [this.props.userSentence],
            isTheGameFinished: false,
            percentageOfDifferences: []
        };
    }


    levenshteinDistance = (value1, value2) => {
        if (!value1.length)
            return value2.length;
        if (!value2.length)
            return value1.length;

        return Math.min(
            this.levenshteinDistance(value1.substr(1), value2) + 1,
            this.levenshteinDistance(value2.substr(1), value1) + 1,
            this.levenshteinDistance(value1.substr(1), value2.substr(1)) + (value1[0] !== value2[0] ? 1 : 0)
        ) + 1;
    }

    _manageResults() {
        //FOR EVERY CONTENT
        this.state.peopleSentences.forEach(async element => {
            const tmpPercentageArray = this.state.percentageOfDifferences;

            tmpPercentageArray.push(this.levenshteinDistance(this.state.userSentence, element));

            this.setState({
                percentageOfDifferences: tmpPercentageArray
            });
        });

        //post the results
        fetch('http://localhost:3002/api/round', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                peopleSentences: this.state.peopleSentences,
                percentageOfDifferences: this.state.percentageOfDifferences,
            })
        })
    }

    _speechToText = async (audio, speakerNumber) => {
        const outputSentence = document.querySelector('#outputSentence');
        if (speakerNumber === 0)
            outputSentence.textContent = 'Processing...';

        await fetch('http://localhost:3002/api/speech-to-text/token').then((response) => {
            return response.text();
        }).then((token) => {
            return recognizeFile({
                token: token,
                file: String(audio.src),
            })
                .promise();
        })
            .then((text) => {
                if (speakerNumber === 0)
                    outputSentence.textContent = "";

                //create elements in div and display them
                const speakerName = document.createElement('b');
                const userName = document.createElement('b');
                const speakerContent = document.createElement('p');
                const userContent = document.createElement('p');

                if (speakerNumber !== 0) {
                    speakerName.innerHTML = "Speaker " + speakerNumber;
                    speakerContent.innerHTML = text;
                }
                else {
                    userContent.innerHTML = this.state.userSentence;
                    userName.innerHTML = String(this.state.userName);
                    outputSentence.appendChild(userName);
                    outputSentence.appendChild(userContent);
                    speakerName.innerHTML = "Speaker " + speakerNumber;
                    speakerContent.innerHTML = text;
                }
                outputSentence.appendChild(speakerName);
                outputSentence.appendChild(speakerContent);


                //create tmpArray to push the new sentence
                const tmpArray = this.state.peopleSentences;
                tmpArray.push(text);
                this.setState({ peopleSentences: tmpArray })
            })
            .catch((err) => {
                console.log(err);
                outputSentence.textContent = err.message;
            });
    }
    _textToSpeech = async (speakerNumber) => {
        await fetch('http://localhost:3002/api/text-to-speech/token')
            .then((response) => {
                return response.text();
            })
            .then((token) => {
                const text =
                    this.state.peopleSentences[speakerNumber];
                return synthesize({
                    text: text,
                    token: token,
                    voice: "es-LA_SofiaVoice",
                    autoPlay: false,
                    accept: 'audio/wav'
                });
            })
            .then(async (audio) => {
                await this._speechToText(audio, speakerNumber);
            });
    }

    _setAllTheRoundsText = async () => {
        for (let i = 0; i <= this.state.nbOfIterations; i++) {
            await this._textToSpeech(i);
        }

        //when processing finish => parsing of the results and post in DB
        this._manageResults();
        this.setState({ isTheGameFinished: true });
    }

    _reloadGame() {
        window.location.reload();
    }

    render() {
        const { classes } = this.props;
        return (
            <div className={classes.roundDiv} >
                <div id="outputSentence" className={classes.outputSentence}>
                </div>
                < div className={classes.button} >
                    {this.state.isTheGameFinished ?
                        <Button
                            onClick={this._reloadGame}
                            variant="contained"
                            color="primary"
                            disableRipple
                            className={classNames(classes.margin, classes.bootstrapRoot)}>
                            RESTART THE GAME
                        </Button>
                        :
                        <Button
                            onClick={this._setAllTheRoundsText}
                            variant="contained"
                            color="primary"
                            disableRipple
                            ref="process"
                            className={classNames(classes.margin, classes.bootstrapRoot)}>
                            STARTING PROCESS
                        </Button>
                    }
                </div >
            </div >
        );
    }
}

export default withStyles(styles, { withTheme: true })(Rounds);
