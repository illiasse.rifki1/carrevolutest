import React, { Component } from 'react';
import Rounds from '../Rounds/rounds';

//Material-ui import
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';

import synthesize from 'watson-speech/text-to-speech/synthesize';

const styles = theme => ({
    textField: {
        color: "white",
    },
    inputDiv: {
        backgroundColor: "transparent",
        margin: 0,
        marginTop: "5%",
        width: "50%",
        float: "right",
        marginRight: "10%",
        textAlign: "center",

    },
    inputTitle: {
        fontSize: "20px",
        color: "#9A4600",
    },
    button: {
        textAlign: "center",
    },
    bootstrapRoot: {
        boxShadow: 'none',
        textTransform: 'none',
        fontSize: 25,
        marginTop: "10%",
        textAlign: "center",
        padding: '12px',
        backgroundColor: '#F6CF70',
        borderRadius: "0",
        borderColor: '#9A4600',
        fontFamily: [
            '-apple-system',
            'BlinkMacSystemFont',
            '"Segoe UI"',
            'Roboto',
            '"Helvetica Neue"',
            'Arial',
            'sans-serif',
            '"Apple Color Emoji"',
            '"Segoe UI Emoji"',
            '"Segoe UI Symbol"',
        ].join(','),
        '&:hover': {
            backgroundColor: '#9A4600',
            borderColor: '#F6CF70',
        },
        '&:active': {
            boxShadow: 'none',
            backgroundColor: '#0062cc',
            borderColor: '#005cbf',
        },
        '&:focus': {
            boxShadow: '0 0 0 0.2rem rgba(0,123,255,.5)',
        },
    },
});

class Input extends Component {
    constructor(props) {
        super();
        this.state = {
            nbOfIterations: 1,
            userSentence: "",
            beginRounds: false
        };
    }

    _changeNbOfIterations = event => {
        if (event.target.value > 0 && event.target.value < 11) {
            this.setState({ nbOfIterations: event.target.value, });
        }
    }
    _changeUserSentence = event => {
        if (event.target.value != null) {
            this.setState({ userSentence: event.target.value });
        }
        else {
            this.setState({
                userSentence: "Hello my name is Illiasse"
            });
        }
    }
    _setBeginRounds = () => {
        fetch('http://localhost:3002/api/text-to-speech/token')
            .then((response) => {
                return response.text();
            })
            .then((token) => {
                const text =
                    "Press starting process to begin the conversation.."
                // String(this.state.userSentence + " ? That's a good sentence to begin the game, lets see how people will change it !");

                synthesize({
                    text: text,
                    token: token,
                    autoPlay: true,
                });
            })
        this.setState({
            beginRounds: true
        });
    }

    render() {
        const { classes } = this.props;

        return (
            <div>
                {(this.state.beginRounds === false) ?
                    < div className={classes.inputDiv}>
                        <h1 className={classes.inputTitle}>
                            Your sentence :
                    </h1>
                        <TextField
                            id="multiline-static"
                            multiline
                            rows="4"
                            placeholder="Hello nice to meet you !"
                            className={classes.textField}
                            margin="normal"
                            value={this.state.userSentence}
                            onChange={this._changeUserSentence}
                        />
                        <h1 className={classes.inputTitle}>
                            Number of iterations
                    </h1>
                        <TextField
                            id="number"
                            value={this.state.nbOfIterations}
                            onChange={this._changeNbOfIterations}
                            type="number"
                            className={classes.textField}
                            InputLabelProps={{
                                shrink: true,
                            }}
                            margin="normal"
                        />
                        <div>
                            <div className={classes.button}>
                                <Button
                                    onClick={this._setBeginRounds}
                                    variant="contained"
                                    color="primary"
                                    disableRipple
                                    className={classNames(classes.margin, classes.bootstrapRoot)}>
                                    SEND SENTENCE
                                </Button>
                            </div>
                        </div >
                    </div >
                    :
                    < Rounds
                        userName={this.props.userName}
                        nbOfIterations={this.state.nbOfIterations}
                        userSentence={this.state.userSentence}
                    />
                }
            </div >
        );
    }
}
export default withStyles(styles, { withTheme: true })(Input);
