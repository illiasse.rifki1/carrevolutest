import React, { Component } from 'react';
import Input from './Input/input';

//Material-ui import
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';

//Watson import
import synthesize from 'watson-speech/text-to-speech/synthesize';

const styles = theme => ({
  header: {
    backgroundColor: "#F6CF70",
    border: "1px solid #F6CF70",
    margin: 0,
    marginTop: "5%",
    width: "50%",
    float: "right",
    marginRight: "10%"
  },
  appTitle: {
    textAlign: "center",
    color: "white",
    fontSize: "60px",
    textShadow: "-1px 0 #9A4600, 0 1px #9A4600, 1px 0 #9A4600, 0 -1px #9A4600",
  },
  rulesDiv: {
    backgroundColor: "transparent",
    margin: 0,
    marginTop: "5%",
    width: "50%",
    float: "right",
    marginRight: "10%"
  },
  rulesText: {
    textAlign: "center",
    color: "#9A4600",
    fontSize: "20px",
    textShadow: "-1px 0 white, 0 1px white, 1px 0 white, 0 -1px white",
  },
  button: {
    padding: 0,
    margin: 0,
    textAlign: "center",
  },
  textFieldDiv:
    {
      width: "100%",
      textAlign: "center",
      margin: 0,
      marginTop: "8%",
      padding: 0
    },
  textField: {
    textAlign: "center",
    width: "30%",
    margin: 0,
  },

  bootstrapRoot: {
    boxShadow: 'none',
    textTransform: 'none',
    fontSize: 25,
    marginTop: "10%",
    textAlign: "center",
    padding: '12px',
    backgroundColor: '#F6CF70',
    borderRadius: "0",
    fontFamily: [
      '-apple-system',
      'BlinkMacSystemFont',
      '"Segoe UI"',
      'Roboto',
      '"Helvetica Neue"',
      'Arial',
      'sans-serif',
      '"Apple Color Emoji"',
      '"Segoe UI Emoji"',
      '"Segoe UI Symbol"',
    ].join(','),
    '&:hover': {
      backgroundColor: '#9A4600',
      borderColor: '#F6CF70',
    },
    '&:active': {
      boxShadow: 'none',
      backgroundColor: '#0062cc',
      borderColor: '#005cbf',
    },
    '&:focus': {
      boxShadow: '0 0 0 0.2rem rgba(0,123,255,.5)',
    },
  },
});

class App extends Component {
  constructor() {
    super();
    this.state = {
      beginGame: false,
      beginRounds: false,
      nbOfIterations: 1,
      userSentence: "",
      userName: "",
    };
  }

  _beginTheGame = () => {
    this._sayUsername();
    this.setState({ beginGame: true });
  }
  _restart = () => {
    this.setState({ beginRounds: false, beginGame: false });
  }
  _setUserName = event => {
    this.setState({ userName: event.target.value })
  }

  _setUserSentence(value) {
    this.setState({ userSentence: value });
  }
  _setNbOfIterations(value) {
    this.setState({ nbOfIterations: value });
  }
  _setBeginRounds() {
    this.setState({
      beginRounds: true,
    });
  }

  _sayUsername() {
    fetch('http://localhost:3002/api/text-to-speech/token')
      .then((response) => {
        return response.text();
      })
      .then((token) => {
        var text =
          String("Welcome in Carrevolutest " + this.state.userName +
            ". To begin the game, enter a sentence and a number of iterations and then press the button : send sentence ")
        synthesize({
          text: text,
          token: token,
          autoPlay: true,
        });
      })
  };

  render() {
    const { classes } = this.props;

    return (

      <div className="App">
        <div className={classes.header}>
          <h1 className={classes.appTitle}>Welcome on carrevolutest !</h1>
        </div>

        {
          (this.state.beginGame === false) &&
          <div className={classes.rulesDiv}>
            <p className={classes.rulesText}>
              The rules of this game are very simple, you just have to enter a sentence (or a paragraph) in the text field and a number of people in the game.<br /><br />
              After that, your sentence will be sent to every people and each one will change a little bit your sentence to make it funnier.<br /><br />
              You just have to click on "Starting process" to see what the people have to say, you will have a tab with all the sentences.
          </p>
            <div className={classes.button}>

              <div className={classes.textFieldDiv}>
                <TextField
                  label="Username"
                  id="name"
                  value={this.state.userName}
                  onChange={this._setUserName}
                  className={classes.textField}
                  margin="normal"
                />
              </div>
              <Button
                onClick={this._beginTheGame}
                variant="contained"
                color="primary"
                disableRipple
                className={classNames(classes.margin, classes.bootstrapRoot)}
              >
                BEGIN THE GAME
      </Button>
            </div>
          </div>
        }

        {
          (this.state.beginGame === true) &&
          <Input
            userName={this.state.userName} />
        }

      </div >
    );
  }
}

export default withStyles(styles, { withTheme: true })(App);
