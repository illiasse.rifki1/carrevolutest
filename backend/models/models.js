var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var RoundSchema = new Schema({
    peopleSentences: { type: [String], required: true },
    percentageOfDifferences: { type: [Number], default: 0 }
},
    { versionKey: false }
);

module.exports = mongoose.model('Round', RoundSchema);