'use strict';

var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var Round = require('../models/models');

mongoose.connect("mongodb://mongodb:27017");
// Get all the rounds in DB
router.get('/round', function (req, res, next) {
    Round.find()
        .then(function (data) {
            res.json(data);
        });
});

// Get one rounds in DB
router.get('/round/:id', function (req, res, next) {
    var _id = req.params.id;
    Round.findById(_id, function (err, data) {
        if (err) {
            res.status(404).send();
        } else {
            res.json(data);
        }
    });
});

// Add a new round
router.post('/round', function (req, res, next) {
    req.accepts('application/json');
    var item = {
        peopleSentences: req.body.peopleSentences,
        percentageOfDifferences: req.body.percentageOfDifferences,
    };

    var data = new Round(item);
    data.save(function (err) {
        if (err) {
            res.status(500).send();
        } else {
            res.status(201).send(data._id);
        }
    });
});

// Delete round
router.delete('/round/:id', function (req, res, next) {
    var _id = req.params.id;
    Round.findByIdAndRemove(_id, function (err, data) {
        if (err) {
            res.status(404).send();
        } else {
            res.status(204).send();
        }
    });
});

// Update round
router.put('/round/:id', function (req, res, next) {
    req.accepts('application/json');
    var _id = req.params.id;
    Round.findById(_id, function (err, data) {
        if (err) {
            res.status(404).send();
        } else {
            data.peopleSentences = req.body.peopleSentences;
            data.percentageOfDifferences = req.body.percentageOfDifferences;
            data.save();
            res.status(200).json(data);
        }
    });
});

module.exports = router;