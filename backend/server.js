//EXPRESS
const express = require('express');
const app = express();
const cors = require('cors');

var bodyParser = require('body-parser');
var round = require('./routes/Api');

//CONFIG FILE
const config = require('./config');

//IBM API
const watson = require('watson-developer-cloud');
const vcapServices = require('vcap_services');

// Middleware to connect express
app.use(cors());

// Body parser middleware connect mongoDB
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use('/api', round);


// Getting authentification token from "Watson speech-to-text" IBM's api and post it
var speechToTextAuth = new watson.AuthorizationV1(
  Object.assign(
    {

      username: config.SPEECH_TO_TEXT_USERNAME,
      password: config.SPEECH_TO_TEXT_PASSWORD
    },
    vcapServices.getCredentials('speech_to_text') // pulls credentials from environment in bluemix, otherwise returns {}
  )
);
app.use('/api/speech-to-text/token', function (req, res) {
  speechToTextAuth.getToken(
    {
      url: watson.SpeechToTextV1.URL
    },
    function (err, token) {
      if (err) {
        console.log('Error on speech to text token: ', err);
        res.status(500).send('Trying again to send token..');
        return;
      }
      res.send(token);
    }
  );
});

// Getting authentification token from "Watson text-to-speech" IBM's api and post it
var textToSpeechAuth = new watson.AuthorizationV1(
  Object.assign(
    {

      username: config.TEXT_TO_SPEECH_USERNAME,
      password: config.TEXT_TO_SPEECH_PASSWORD
    },
    vcapServices.getCredentials('text_to_speech')
  )
);
app.use('/api/text-to-speech/token', function (req, res) {
  textToSpeechAuth.getToken(
    {
      url: watson.TextToSpeechV1.URL
    },
    function (err, token) {
      if (err) {
        console.log('Error on text to speech token: ', err);
        res.status(500).send('Trying again to send token..');
        return;
      }
      res.send(token);
    }
  );
});

const port = process.env.PORT || 3002;
app.listen(port, function () {
  console.log('You can open Carrevolutest on this link : http://localhost:%s/', port);
});